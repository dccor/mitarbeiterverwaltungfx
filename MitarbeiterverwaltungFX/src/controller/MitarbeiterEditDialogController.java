package controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Experten;
import model.Manager;
import model.Mitarbeiter;

public class MitarbeiterEditDialogController {

	@FXML
	private TextField idField;
	@FXML
	private TextField bonusField;
	@FXML
	private TextField nameField;
	@FXML
	private DatePicker dtpGebDatum;
	@FXML
	private DatePicker dtpEintrittDatum;
	@FXML
	private TextField positionField;
	@FXML
	private TextField gehaltField;
	// @FXML
	// private TextField birthdayField;
	@FXML
	private TextField fachgebietField;
	// String Property für title
	private StringProperty title = new SimpleStringProperty();;

	private Stage dialogStage;
	private Mitarbeiter mitarbeiter;
	private boolean okClicked = false;

	// title Property
	public StringProperty titleProperty() {
		return title;
	}

	public String getTitle() {
		return title.get();
	}

	public void setTitle(String title) {
		this.title.set(title);
	}

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {
		System.out.println("initialize");
//		idField.setEditable(false);
		positionField.textProperty().addListener((o, oldValue, newValue) -> checkFachGeb());
		positionField.textProperty().addListener((o, oldValue, newValue) -> checkBonus());
		fachgebietField.setVisible(false);
		fachgebietField.setEditable(false);
		bonusField.setVisible(false);
		bonusField.setEditable(false);

		// bind();
	}

	private void checkBonus() {
		if (positionField.getText().equalsIgnoreCase("Manager")) {
			bonusField.setVisible(true);
			bonusField.setEditable(true);
		} else {
			bonusField.setVisible(false);
			bonusField.clear();
		}
	}

	/**
	 * Listener from positionField
	 */
	private void checkFachGeb() {
		if (positionField.getText().equalsIgnoreCase("Experte")) {
			fachgebietField.setVisible(true);
			fachgebietField.setEditable(true);
		} else {
			fachgebietField.setVisible(false);
			fachgebietField.clear();
		}
	}

	/**
	 * Sets the stage of this dialog.
	 * 
	 * @param dialogStage
	 */
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
	}
	
	/**
	 * getter returns Mitarbeiter
	 */
	public Mitarbeiter getMitarbeiter(){
		return this.mitarbeiter;
	}

	/**
	 * Sets the Mitarbeiter to be edited in the dialog.
	 * 
	 * @param mitarb
	 */
	public void setMitarbeiter(Mitarbeiter mitarb) {
		this.mitarbeiter = mitarb;

//		idField.setText(Integer.toString(mitarb.getId()));
		nameField.setText(mitarb.getName());
		dtpGebDatum.setValue(mitarb.getGebDatum());
		dtpEintrittDatum.setValue(mitarb.getEintrDatum());
		positionField.setText(mitarb.getPosition());
		gehaltField.setText(Double.toString(mitarb.getGehalt()));
		if (mitarbeiter.getMitType() == 2) {
			fachgebietField.setText(((Experten) mitarb).getFachGeb());
		}
		if (mitarbeiter.getMitType() == 1) {
			bonusField.setText(Double.toString(((Manager) mitarb).getBonus()));
		}
	}

	/**
	 * Returns true if the user clicked OK, false otherwise.
	 * 
	 * @return
	 */
	public boolean isOkClicked() {
		return okClicked;
	}

	/**
	 * Called when the user clicks ok.
	 */
	@FXML
	private void handleOk() {
		if (isInputValid()) {
			if (positionField.getText().equalsIgnoreCase("Experte")) {
				int id = mitarbeiter.getId();
				mitarbeiter = new Experten();
				mitarbeiter.setId(id);
				((Experten) mitarbeiter).setFachGeb(fachgebietField.getText());
			}
			if (positionField.getText().equalsIgnoreCase("Manager")) {
				int id = mitarbeiter.getId();
				mitarbeiter = new Manager();
				mitarbeiter.setId(id);
				((Manager) mitarbeiter).setBonus(Double.parseDouble(bonusField.getText()));
			}
			mitarbeiter.setName(nameField.getText());
			mitarbeiter.setGebDatum(dtpGebDatum.getValue());
			mitarbeiter.setEintrDatum(dtpEintrittDatum.getValue());
			mitarbeiter.setPosition(positionField.getText());
			mitarbeiter.setGehalt(Double.parseDouble(gehaltField.getText()));

			okClicked = true;
			dialogStage.close();
		}
	}

	/**
	 * Called when the user clicks cancel.
	 */
	@FXML
	private void handleCancel() {
		dialogStage.close();
	}

	/**
	 * Validates the user input in the text fields.
	 * 
	 * @return true if the input is valid
	 */
	private boolean isInputValid() {
		String errorMessage = "";
		if (nameField.getText() == null || nameField.getText().length() <= 3) {
			errorMessage += "Kein gültiger Name!\n";
		}
		if (dtpGebDatum.getValue() == null) {
			errorMessage += "Kein gültiges Datum!\n";
		}
		if (dtpEintrittDatum.getValue() == null) {
			errorMessage += "Kein gültiges Datum!\n";
		}

		if (gehaltField.getText() == null || gehaltField.getText().length() <= 0) {
			errorMessage += "Kein gültiges Gehalt!\n";
		} else {
			// try to parse the postal code into an int.
			try {
				Double.parseDouble(gehaltField.getText());
			} catch (NumberFormatException e) {
				errorMessage += "Kein gültiges Gehalt (Muss positive Zahl sein)!\n";
			}
		}

		if (positionField.getText() == null || positionField.getText().length() == 0) {
			errorMessage += "Keine gültige Position!\n";
		}

		if (errorMessage.length() == 0) {
			return true;
		} else {
			// Show the error message.
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(dialogStage);
			alert.setTitle("Ungültige Felder");
			alert.setHeaderText("Bitte korigieren Sie Felder");
			alert.setContentText(errorMessage);

			alert.showAndWait();

			return false;
		}
	}

}
