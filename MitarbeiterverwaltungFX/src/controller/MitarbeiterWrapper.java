package controller;

import java.time.LocalDate;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.adapter.JavaBeanDoubleProperty;
import javafx.beans.property.adapter.JavaBeanDoublePropertyBuilder;
import javafx.beans.property.adapter.JavaBeanIntegerPropertyBuilder;
import javafx.beans.property.adapter.JavaBeanObjectPropertyBuilder;
import javafx.beans.property.adapter.JavaBeanStringPropertyBuilder;
import model.Mitarbeiter;

public class MitarbeiterWrapper {
	
	private IntegerProperty id;
	private JavaBeanDoubleProperty gehalt;
	private StringProperty name, position, fachGeb;
//	private DoubleProperty gehalt;
	private ObjectProperty<LocalDate> gebDatum;
	private ObjectProperty<LocalDate> eintrittDatum;

	private Mitarbeiter bean;


	public MitarbeiterWrapper(Mitarbeiter value) {
		createWrapper(value);
	}


	@SuppressWarnings("unchecked")
	private void createWrapper(Mitarbeiter value) {
		try {
			this.bean = value;

			id = new JavaBeanIntegerPropertyBuilder().bean(this.bean).name("id").build();
			name = new JavaBeanStringPropertyBuilder().bean(this.bean).name("name").build();
			position = new JavaBeanStringPropertyBuilder().bean(this.bean).name("position").build();
			fachGeb = new JavaBeanStringPropertyBuilder().bean(this.bean).name("fachGeb").build();
			gehalt = new JavaBeanDoublePropertyBuilder().bean(this.bean).name("gehalt").build();
			
			gebDatum = new JavaBeanObjectPropertyBuilder<>().bean(this.bean).name("gebDatum").build();
			eintrittDatum = new JavaBeanObjectPropertyBuilder<>().bean(this.bean).name("eintrittDatum").build();
			
		} catch (NoSuchMethodException ex) {
			System.out.println("Error creating bean wrapper object!!");
			ex.printStackTrace();
		}
		
	}
	
	public Mitarbeiter get() {
		return bean;
	}

	public void set(Mitarbeiter value) {
		createWrapper(value);
	}
	
	/**
	 * PROPERTY und  getter und setter
	 * @return
	 */
	public StringProperty nameProperty() {
		return name;
	}

	public String getName() {
		return name.get();
	}

	public void setName(String name) {
		this.name.set(name);
	}
	public IntegerProperty idProperty() {
		return id;
	}

	public int getId() {
		return id.get();
	}

	public void setId(int id) {
		this.id.set(id);
	}
	public DoubleProperty gehalt() {
		return gehalt();
	}

	public double getGehalt() {
		return gehalt.get();
	}

	public void setGehalt(double gehalt) {
		this.gehalt().set(gehalt);
	}
	public StringProperty positionProperty() {
		return position;
	}

	public String getPosition() {
		return position.get();
	}

	public void setPosition(String p_position) {
		this.position.set(p_position);
	}
	public StringProperty fachGebProperty() {
		return fachGeb;
	}

	public String getFachGeb() {
		return fachGeb.get();
	}

	public void setFachGeb(String p_fachGeb) {
		this.fachGeb.set(p_fachGeb);
	}
	public ObjectProperty<LocalDate> gebDatumProperty() {
		return gebDatum;
	}

	public LocalDate getGebDatum() {
		return gebDatum.get();
	}

	public void setGebDatum(LocalDate gDate) {
		this.gebDatum.set(gDate);
	}
	public ObjectProperty<LocalDate> eintrittDatumProperty() {
		return eintrittDatum;
	}

	public LocalDate getEintrittDatum() {
		return eintrittDatum.get();
	}

	public void setEintrittDatum(LocalDate eDate) {
		this.eintrittDatum.set(eDate);
	}

}
