﻿package controller;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.css.PseudoClass;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;

import java.text.NumberFormat;
public class ValidationBindings {


	public static BooleanBinding createValidationBinding(Control control, BooleanBinding binding, String tooltip) {

		// set the Tooltip
		control.setTooltip(new Tooltip(tooltip));

		// if the value is initially invalid, add pseudo-class
		if (!binding.get()) {
			control.pseudoClassStateChanged(PseudoClass.getPseudoClass("validation-error"), true);
		}

		// add the listener to the boolean binding to toggle the pseudo-class,
		// if the valid state changes
		binding.addListener((o, oldVal, newVal) -> {
			System.out.println("Validation changed for " + control.getId() + ": newVal=" + newVal);
			control.pseudoClassStateChanged(PseudoClass.getPseudoClass("validation-error"), !newVal);
		});

		// return the binding
		return binding;
	}
	
	// eine statische Methode erstellen Validation
	public static BooleanBinding createRequiredTextfieldBinding(TextField textField, String name){
		BooleanBinding binding = Bindings.createBooleanBinding(
				() -> textField.getText() != null && ! textField.getText().isEmpty(),
				// Binding ist abhängig vom Texttyp
				textField.textProperty());
		return createValidationBinding(textField, binding, "Das Feld '" + name + "' ist erforderlich!");
	}
	
	// Methode für validation
	public static BooleanBinding createIntegerValidationBinding(TextField textField){
		BooleanBinding binding = Bindings.createBooleanBinding(
				()-> isValidInteger(textField.getText(), Integer.MIN_VALUE, Integer.MAX_VALUE),
				textField.textProperty());
				
		return createValidationBinding(textField, binding, "Geben Sie bitte eine Ganzzahl ein");
	}
	
	// Methode validation Combobox
	public static BooleanBinding createRequiredFieldBinding(ComboBox<?> cmb, String name){
		BooleanBinding binding = Bindings.createBooleanBinding(
				()-> cmb.getSelectionModel().getSelectedIndex() >= 0,
				cmb.getSelectionModel().selectedIndexProperty());
				
		return createValidationBinding(cmb, binding, "Wählen Sie einen Eintrag aus '" + name + "' aus");
	}
	// Methode validation
	public static BooleanBinding createRequiredFieldBinding(DatePicker dtp, String name){
		BooleanBinding binding = Bindings.createBooleanBinding(
			() ->dtp.getValue() != null, dtp.valueProperty());
		return createValidationBinding(dtp, binding, "Das Feld '" + name + "' ist erforderlich");
	}


	public static boolean isValidInteger(String value, int min, int max) {
		try {
			if (value == null || value.isEmpty()) {
				return false;
			} else {
				int number = Integer.parseInt(value);
				return number >= min && number <= max;
			}
		} catch (Exception ex) {
			System.out.println("Error parsing string to int: " + ex.toString());
			return false;

		}
	}

	// brauchen wir zwar in diesem Beispiel nicht 
	public static boolean isValidDouble(String value, double min, double max) {
		try {
			if (value == null || value.isEmpty()) {
				return false;
			} else {
				NumberFormat fmt = NumberFormat.getInstance();
				fmt.setMinimumFractionDigits(0);
				fmt.setMaximumFractionDigits(2);

				double number = fmt.parse(value).doubleValue();
				return number >= min && number <= max;
			}
		} catch (Exception ex) {
			System.out.println("Error parsing string to double: " + ex.toString());
			return false;

		}
	}
}
