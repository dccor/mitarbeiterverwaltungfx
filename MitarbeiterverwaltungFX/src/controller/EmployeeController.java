package controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.sun.org.apache.xerces.internal.impl.dtd.models.CMLeaf;

import application.Main;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import model.Experten;
import model.Manager;
import model.Mitarbeiter;
import model.MitarbeiterCellFactory;
import model.MitarbeiterListe;

public class EmployeeController {
	// Instanz für binding
	// private MitarbeiterWrapper mitarbeiter;
	// String Property für title
	// private StringProperty title;
	// Property für validation
	// private BooleanProperty isValid;

	@FXML
	private ListView<Mitarbeiter> mitListView;
	@FXML
	private TableColumn<Mitarbeiter, Integer> idColumn;
	@FXML
	private TableColumn<Mitarbeiter, String> nameColumn;
	@FXML
	private TableColumn<Mitarbeiter, LocalDate> gebDatumColumn;
	@FXML
	private ComboBox<String> cmbSortiereNach;
	@FXML
	private Label idLabel;
	@FXML
	private Label nameLabel;
	@FXML
	private Label gebDatumLabel;
	@FXML
	private Label eintrittDatumLabel;
	@FXML
	private Label positionLabel;
	@FXML
	private Label gehaltLabel;
	@FXML
	private Label jahrGehaltLabel;
	@FXML
	private Label fachGebLabel;
	@FXML
	private Label bonusLabel;
	@FXML
	private TextField sucheNach;
	@FXML
	Node boxSelMitarbeiter;
	// Property für Miatrbreiterliste
	private ListProperty<Mitarbeiter> mitListProperty;
	// Property für Selected Mitarbeiter
	private ObjectProperty<Mitarbeiter> selectedMitarbeiter;
	private Main main;

	/**
	 * The constructor. The constructor is called before the initialize()
	 * method.
	 */
	public EmployeeController() {
		mitListProperty = new SimpleListProperty<>();
		selectedMitarbeiter = new SimpleObjectProperty<>();
	}

	// für die Liste genügt ein getter fürs Binding
	public ObservableList<Mitarbeiter> getMitarbeiter() {
		return mitListProperty;
	}

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {
		// sucheNach.setTooltip(new Tooltip("Eingabe für Mitarbeiter
		// Sortierung"));

		mitListView.getItems().addAll(mitListProperty);
		mitListView.setCellFactory(MitarbeiterCellFactory.defaultMitarbeiterDisplay());
		// Listen for selection changes and show the mitarbeiter details when
		// changed.
		mitListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			if (selectedMitarbeiter.get() != newValue) {
				selectedMitarbeiter.set(newValue);
			}
		});
		// die visibility der Box anpassen
		boxSelMitarbeiter.visibleProperty().bind(Bindings.isNotNull(selectedMitarbeiter));
		// Listen for selection changes and show the person details when
		// changed.
		mitListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue != null)
				showMitarbeiterDetails(newValue);
		});

		// sucheNach.textProperty().addListener((o, oldValue, newvalue) ->
		// doSort());
		// Item in der Combobox hinzufügen und Lister für doSort
		cmbSortiereNach.getItems().add("Name");
		cmbSortiereNach.getItems().add("Position");
		cmbSortiereNach.getItems().add("Id");
		cmbSortiereNach.getSelectionModel().select(0);
		cmbSortiereNach.getSelectionModel().selectedItemProperty().addListener((o, oldValue, newValue) -> doSort());

	}

	private void showMitarbeiterDetails(Mitarbeiter mitarbeiter) {
		DateTimeFormatter formatterOut = DateTimeFormatter.ofPattern("dd.MM.yyyy");
		String edatum = formatterOut.format(mitarbeiter.getEintrDatum());
		String gdatum = formatterOut.format(mitarbeiter.getGebDatum());

		idLabel.setText(Integer.toString(mitarbeiter.getId()));
		nameLabel.setText(mitarbeiter.getName());
		gebDatumLabel.setText(gdatum);
		eintrittDatumLabel.setText(edatum);
		positionLabel.setText(mitarbeiter.getPosition());
		gehaltLabel.setText(Double.toString(mitarbeiter.getGehalt()));
		jahrGehaltLabel.setText(Double.toString(mitarbeiter.jahrGehalt()));
		if (mitarbeiter.getMitType() == 2) {
			fachGebLabel.setText(((Experten) mitarbeiter).getFachGeb());
		} else
			fachGebLabel.setText("");
		if (mitarbeiter.getMitType() == 1) {
			bonusLabel.setText(Double.toString(((Manager) mitarbeiter).getBonus()));
		} else
			bonusLabel.setText("");
	}

	/**
	 * Is called by the main application to give a reference back to itself.
	 * 
	 * @param mainApp
	 */
	public void setMain(Main main) {
		this.main = main;

	}

	/**
	 * Called when the user clicks on the delete button.
	 */
	@FXML
	private void handleDeleteMitarbeiter() {
		int selectedIndex = mitListView.getSelectionModel().getSelectedIndex();
		if (selectedIndex >= 0) {
			mitListView.getItems().remove(selectedIndex);
			mitListProperty.get().remove(selectedIndex);
		} else {
			// Nothing selected.
			Alert alert = new Alert(AlertType.WARNING);
			alert.initOwner(main.getPrimaryStage());
			alert.setTitle("Nichts ausgewählt");
			alert.setHeaderText("Kein Mitarbeiter ausgewählt");
			alert.setContentText("Bitte einen Mitarbeiter aus Liste auswählen");

			alert.showAndWait();
		}
	}

	/**
	 * Called when the user clicks the new button. Opens a dialog to edit
	 * details for a new person.
	 */
	@FXML
	private void handleNewMitarbeiter() {
		// leeren Mitarbeiter Instanzieren
		Mitarbeiter tempPerson = new Mitarbeiter();
		tempPerson = main.showPersonEditDialog(tempPerson);
		if (tempPerson != null) {
			tempPerson.setId(main.getMitarbeiterliste().getHighestId() + 1);
			main.getMitarbeiterliste().getMitarbeiterData().add(tempPerson);
			mitListView.setItems(mitListProperty);
			mitListView.refresh();
			// KONTROLL
			// ----------------------------------------------------------
			System.out.println(
					"main.getMitarbeiterliste().getMitarbeiterData().toString()): " + main.getMitarbeiterliste().getMitarbeiterData().toString());
		}
	}

	/**
	 * Called when the user clicks the edit button. Opens a dialog to edit
	 * details for the selected person.
	 */
	@FXML
	private void handleEditMitarbeiter() {
		Mitarbeiter selectedPerson = mitListView.getSelectionModel().getSelectedItem();
		if (selectedPerson != null) {
			selectedPerson = main.showPersonEditDialog(selectedPerson);
			if (selectedPerson != null) {
				showMitarbeiterDetails(selectedPerson);
				int selMit = mitListView.getSelectionModel().getSelectedIndex();
				main.getMitarbeiterliste().getMitarbeiterData().set(selMit, selectedPerson);
				System.out.println("SelMit: " + selMit + "selectedPerson" + selectedPerson.toString());
				// mitListView.setItems(mitListProperty);
				mitListView.refresh();
				// KONTROLL :
				System.out.println("mitListView: " + mitListView.getItems().toString());
			}

		} else {
			// Nothing selected.
			Alert alert = new Alert(AlertType.WARNING);
			alert.initOwner(main.getPrimaryStage());
			alert.setTitle("Nichts ausgewählt!");
			alert.setHeaderText("Kein Mitarbeiter ausgewählt!");
			alert.setContentText("Bitte einen Mitarbeiter aus Liste auswählen!");

			alert.showAndWait();
		}
	}

	/**
	 * sortiereNach implementierung
	 */
	@FXML
	public void doSort() {
		try {
			if (cmbSortiereNach.getSelectionModel().getSelectedItem().equals("Name")) {
				main.getMitarbeiterliste().sortList();
				mitListView.refresh();
				// ----------------- KONTROLL --------------------------
				System.out.println("sortList - Name" + main.getMitarbeiterliste());
			}
			if (cmbSortiereNach.getSelectionModel().getSelectedItem().equals("Position")) {
				main.getMitarbeiterliste().sortByTypEDatum();
				mitListView.refresh();

				System.out.println(mitListProperty.toString());
			}
			if (cmbSortiereNach.getSelectionModel().getSelectedItem().equals("Id")) {
				// TODO : sort by id
				main.getMitarbeiterliste().sortById();
				mitListView.refresh();
				// KONTROLL -------------------------------

			}
		} catch (Exception e) {
			System.out.println("Sortieren FAHLER!");
			e.printStackTrace();
		}
	}

}
