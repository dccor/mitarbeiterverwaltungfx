package view;

import java.io.File;
import java.util.Optional;

import application.EmployeeIO;
import application.Main;
import controller.EmployeeController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import model.MitarbeiterListe;

public class MenuBar {
	@FXML
	private BorderPane mainPane;
	@FXML
	private MenuItem cmdQuit;
	private FileChooser dlg;
	// Objekt mitarbeiterListe for Menue Verwalten - New
	private MitarbeiterListe listData;
	// Reference to the main application.
	private Main main;

	@FXML
	private void initialize() {
		dlg = createTextFileChooser("");
	}

	/**
	 * FileChooser with Dialog dlg and InitialDirectory and ExtensionFilter
	 * 
	 * @param title
	 * @return
	 */
	private FileChooser createTextFileChooser(String title) {
		FileChooser dlg = new FileChooser();
		dlg.setTitle(title);
		dlg.setInitialDirectory(new File("."));
		// Filterstring muss den * enthalten
		dlg.getExtensionFilters().add(new ExtensionFilter("XML files", "*.xml"));
		dlg.getExtensionFilters().add(new ExtensionFilter("All files", "*"));
		return dlg;
	}

	// @FXML
	// public void onOpen(ActionEvent event) {
	// dlg.setTitle("Open file");
	// File selFile = dlg.showOpenDialog(mainPane.getScene().getWindow());
	// if (selFile != null) {
	// EmployeeIO.openMitarbeiterXml(listData, selFile.getAbsolutePath());
	// // KONTROLL ----------------------------------
	// System.out.println("onOpen");
	// } else {
	// System.out.println("Open cancelled");
	// }
	// }
	// Version 2 (Persons)
	@FXML
	private void onOpen(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
		dlg.setTitle("Open file");

		// Set extension filter
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
		fileChooser.getExtensionFilters().add(extFilter);
		// Show open file dialog
		File file = fileChooser.showOpenDialog(mainPane.getScene().getWindow());
		if (file != null) {
			main.setMitarbeiterliste(EmployeeIO.loadMitarbeiterDataFromFile(file));
		}
	}

	@FXML
	private void onSave(ActionEvent event) {

		dlg.setInitialFileName("Unbenannt.txt");
		dlg.setTitle("Save File");
		File selFile = dlg.showSaveDialog(mainPane.getScene().getWindow());
		if (selFile != null) {
			dlg.setInitialDirectory(selFile.getParentFile());
			System.out.println("OnSave: selected file=" + selFile.getAbsolutePath());
			EmployeeIO.saveMitarbeiterXml(listData, selFile.getPath(), "UTF-8");
			System.out.println("name: " + selFile.getName() + "getPath:" + selFile.getPath());
		} else {
			System.out.println("OnSave cancelled");
		}
	}

	@FXML
	private void onQuit(ActionEvent event) {
		quit();
	}

	/**
	 * Quit Method with Confirmation asked
	 */
	public void quit() {
		Alert question = new Alert(AlertType.CONFIRMATION, "Are you sure?", ButtonType.OK, ButtonType.CANCEL);

		question.setTitle("Quit");
		question.setHeaderText(null);
		// den Button
		Button btnOK = (Button) question.getDialogPane().lookupButton(ButtonType.OK);
		btnOK.setText("Confirm");
		btnOK.setDefaultButton(false);
		Button btnCancel = (Button) question.getDialogPane().lookupButton(ButtonType.CANCEL);
		btnCancel.setText("Cancel");
		btnCancel.setDefaultButton(true);
		Optional<ButtonType> result = question.showAndWait();

		if (result.isPresent() && result.get().equals(ButtonType.OK)) {
			// wirklich beenden: das Hauptfenster auf Stage casten und schließen
			((Stage) mainPane.getScene().getWindow()).close();
		}
	}
	public void setMain(Main main) {
		this.main = main;

	}

	/**
	 * Called when the user clicks the new button. Opens a dialog to edit
	 * details for a new person.
	 */
	// @FXML
	// private void handleNewMitarbeiter() {
	// Mitarbeiter tempPerson = new Mitarbeiter();
	// boolean okClicked = main.showPersonEditDialog(tempPerson);
	// if (okClicked) {
	// mitarbeiterListe.getMitarbeiterData().add(tempPerson);
	//
	// }
	// }
}
