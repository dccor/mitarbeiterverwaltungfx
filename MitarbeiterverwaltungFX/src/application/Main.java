package application;

import java.io.IOException;

import controller.EmployeeController;
import controller.MitarbeiterEditDialogController;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Mitarbeiter;
import model.MitarbeiterListe;
import view.MenuBar;

public class Main extends Application {

	private Stage primaryStage;
	private BorderPane rootLayout;
	private EmployeeController econtroller;
	private MenuBar mbcontroller;
	private MitarbeiterListe mitarbeiterliste;
	private ObservableList<Mitarbeiter> dataList;
	

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Employee");

		initRootLayout();
		showEmployee();
	}

	/**
	 * Initializes the root layout.
	 */
	public void initRootLayout() {
		try {
			// Load root layout from fxml file.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("../view/RootLayout.fxml"));
			rootLayout = (BorderPane) loader.load();
			mbcontroller = loader.getController();
			mbcontroller.setMain(this);

			// Show the scene containing the root layout.
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.show();
			
			primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("../view/Mitarb_verwaltung-icon.png")));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Try to load last opened mitarbeiter file.
//	    File file = EmployeeIO.getMitarbeiterFilePath();
//	    if (file != null) {
//	    	// TODO: ?????
//	        EmployeeIO.loadPersonDataFromFile(mitList, file);
//	    }
	}

	/**
	 * Shows the employee inside the root layout.
	 */
	public void showEmployee() {
		try {
			// Load person overview.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("../view/Employee.fxml"));
			AnchorPane employee = (AnchorPane) loader.load();

			// Set mitarbeiter overview into the center of root layout.
			rootLayout.setCenter(employee);
			
			// Give the controller access to the main app.
	        econtroller = loader.getController();
	        econtroller.setMain(this);
	        this.mitarbeiterliste = new MitarbeiterListe();
	        this.dataList = FXCollections.observableArrayList();
//	        KONTROLL ----------------------------------------------------------
	        System.out.println(dataList);
	        
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Opens a dialog to edit details for the specified person. If the user
	 * clicks OK, the changes are saved into the provided person object and true
	 * is returned.
	 * 
	 * @param person the person object to be edited
	 * @return true if the user clicked OK, false otherwise.
	 */
	public Mitarbeiter showPersonEditDialog(Mitarbeiter mitarb) {
	    try {
	        // Load the fxml file and create a new stage for the popup dialog.
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(Main.class.getResource("../view/MitarbeiterEditDialog.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();

	        // Create the dialog Stage.
	        Stage dialogStage = new Stage();
	        if(mitarb.getId() == 0){
	        	dialogStage.setTitle("Mitarbeiter erstellen");
	        }else{
	        dialogStage.setTitle("Mitarbeiter bearbeiten");
	        }
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(primaryStage);
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);

	        // Set the person into the controller.
	        MitarbeiterEditDialogController controller = loader.getController();
	        controller.setDialogStage(dialogStage);
	        controller.setMitarbeiter(mitarb);
	        if(mitarb.getId() == 0){
	        	controller.setTitle("Mitarbeiter erstellen");
	        }else{
	        	controller.setTitle("Mitarbeiter bearbeiten");
	        }

	        // Show the dialog and wait until the user closes it
	        dialogStage.showAndWait();
	     // Set the dialog icon.
//            dialogStage.getIcons().add(new Image("file:Icon/Mitarb_edit-icon.png"));
            dialogStage.getIcons().add(new Image(getClass().getResourceAsStream("../view/Mitarb_edit-icon_52x50.png")));

	        if(controller.isOkClicked()){
	        	return controller.getMitarbeiter();
	        }else
	        	return null;
	    } catch (IOException e) {
	        e.printStackTrace();
	        return null;
	    }
	}
	/**
	 * getter,setter Mitarbeiterliste
	 * @return
	 */
	public MitarbeiterListe getMitarbeiterliste(){
		return mitarbeiterliste;
	}
	public void setMitarbeiterliste(MitarbeiterListe mitListe){
		this.mitarbeiterliste = mitListe;
	}

	/**
	 * Returns the main stage.
	 * 
	 * @return
	 */
	public Stage getPrimaryStage() {
		return primaryStage;
	}
	
	public EmployeeController getEcontroller() {
		return econtroller;
	}
	public MenuBar getMbcontroller(){
		return mbcontroller;
	}

	public static void main(String[] args) {
		
		launch(args);
	}
}
