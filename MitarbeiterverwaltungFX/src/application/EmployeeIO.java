package application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.prefs.Preferences;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import model.MitarbeiterListe;

public class EmployeeIO {
	private static Main main;

	public static boolean openMitarbeiterXml(MitarbeiterListe mitList, String fileName) {
		boolean isopen = false;

		try (InputStreamReader reader = new InputStreamReader(new FileInputStream(fileName))) {
			JAXBContext context = JAXBContext.newInstance(MitarbeiterListe.class);
			Unmarshaller u = context.createUnmarshaller();
			MitarbeiterListe open = (MitarbeiterListe) u.unmarshal(reader);
			// TODO: ????????
			mitList.getMitarbeiterData().addAll(open.getMitarbeiterData());
			mitList.getMitarbeiterData().addAll(open.getMitarbeiterData());

			isopen = true;
		} catch (Exception e) {
			System.out.println("Fehler beim Laden");
			e.printStackTrace(System.out);
		}
		return isopen;
	}

	public static boolean saveMitarbeiterXml(MitarbeiterListe mitList, String fileName, String... encoding) {
		String saveFile = "";
		String encode = "UTF-8";
		if (encoding.length > 0)
			encode = encoding[0];
		if (fileName.isEmpty()) {
			saveFile = ("Xml_MitarbeiterListe_0.xml");
		} else
			saveFile = fileName;

		try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(saveFile), encode)) {
			// einen JAAX-B Context für unser Root element erstellen
			JAXBContext context = JAXBContext.newInstance(MitarbeiterListe.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.setProperty(Marshaller.JAXB_ENCODING, encode);
//			System.out.println("mitList" + mitList.toString());
			m.marshal(mitList, writer);
			return true;
		} catch (Exception e) {
			System.out.println("Fehler beim Speichern");
			e.printStackTrace(System.out);
		}
		return false;
	}

	/**
	 * Returns the person file preference, i.e. the file that was last opened.
	 * The preference is read from the OS specific registry. If no such
	 * preference can be found, null is returned.
	 * 
	 * @return
	 */
	public static File getMitarbeiterFilePath() {
		Preferences prefs = Preferences.userNodeForPackage(Main.class);
		String filePath = prefs.get("filePath", null);
		if (filePath != null) {
			return new File(filePath);
		} else {
			return null;
		}
	}

	/**
	 * Sets the file path of the currently loaded file. The path is persisted in
	 * the OS specific registry.
	 * 
	 * @param file
	 *            the file or null to remove the path
	 */
	public static void setMitarbeiterFilePath(File file) {
		Preferences prefs = Preferences.userNodeForPackage(Main.class);
		if (file != null) {
			prefs.put("filePath", file.getPath());

			// Update the stage title.
			 main.getPrimaryStage().setTitle("Employee - " + file.getName());
			 } else {
			 prefs.remove("filePath");
			 // Update the stage title.
			 main.getPrimaryStage().setTitle("Employee");
			 }
	}
	/**
	 * Loads person data from the specified file. The current person data will
	 * be replaced.
	 * 
	 * @param file
	 * @return 
	 */
	public static MitarbeiterListe loadMitarbeiterDataFromFile(File file) {
	    MitarbeiterListe mitListe = new MitarbeiterListe();
		try {
	        JAXBContext context = JAXBContext
	                .newInstance(MitarbeiterListe.class);
	        Unmarshaller um = context.createUnmarshaller();

	        // Reading XML from the file and unmarshalling.
	        mitListe = (MitarbeiterListe) um.unmarshal(file);
	        setMitarbeiterFilePath(file);

	    } catch (Exception e) { // catches ANY exception
	        Alert alert = new Alert(AlertType.ERROR);
	        alert.setTitle("Error");
	        alert.setHeaderText("Could not load data!!");
	        alert.setContentText("Could not load data from file:\n" + file.getPath());
	        //  KONTROLL :
	        System.out.println(e.getMessage());
	        alert.showAndWait();
	    }
		return mitListe;
	}

	/**
	 * Saves the current person data to the specified file.
	 * 
	 * @param file
	 */
	public static void saveMitarbeiterDataToFile(MitarbeiterListe mitList, File file) {
	    try {
	        JAXBContext context = JAXBContext
	                .newInstance(MitarbeiterListe.class);
	        Marshaller m = context.createMarshaller();
	        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

	        // Marshalling and saving XML to the file.
	        m.marshal(mitList, file);

	        // Save the file path to the registry.
	        setMitarbeiterFilePath(file);
	    } catch (Exception e) { // catches ANY exception
	        Alert alert = new Alert(AlertType.ERROR);
	        alert.setTitle("Error");
	        alert.setHeaderText("Could not save data");
	        alert.setContentText("Could not save data to file:\n" + file.getPath());

	        alert.showAndWait();
	    }
	}
}