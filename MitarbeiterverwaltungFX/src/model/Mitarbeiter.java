package model;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Mitarbeiter implements Comparable<Mitarbeiter> {

//	@XmlElement
	protected IntegerProperty id = new SimpleIntegerProperty();

	protected StringProperty name = new SimpleStringProperty("");

    protected ObjectProperty<LocalDate> gDatum = 
    		new SimpleObjectProperty<LocalDate>();

	protected ObjectProperty<LocalDate> eDatum = 
    		new SimpleObjectProperty<LocalDate>();
	
	protected DoubleProperty gehalt = new SimpleDoubleProperty();
	protected StringProperty position = new SimpleStringProperty("");
	
	static int cnt = 0;

	/**
	 * 
	 * @param name
	 * @param gehalt
	 * @param gebDatum
	 * @param eintrittDatum
	 */
	public Mitarbeiter(int id, String name, double gehalt, LocalDate gebDatum, LocalDate eintrittDatum, String position) {
		this.id.set(id);
		this.name.set(name);
		this.gehalt.set(gehalt);
		this.gDatum.set(gebDatum);
		this.eDatum.set(eintrittDatum);
		this.position.set(position);
	}
	
	public Mitarbeiter(){
		
	}

	/**
	 * 
	 * @return Mitarbeiter-Typ als int
	 */
	public int getMitType() {
		return 0;
	}
	/**
	 *  
	 * @return  Position Mitarbeiter
	 */
	public String getPosition(){
		return this.position.get();
	}
	public void setPosition(String position){
		this.position.set(position);
	}
	public StringProperty positionProperty() {
        return position;
	}
    
	
	/**
	 * 2 camparable Mitarbeiten vergleichen
	 */
	@Override
	public int compareTo(Mitarbeiter arg0) {
	return name.get().compareTo(arg0.getName());
	}
	
	
	/**
	 * PROPERTY für #ID
	 * @return
	 */
	public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public IntegerProperty idProperty() {
        return id;
    }
    /**
     * PROPERTY für name
     * @return
     */
    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty() {
        return name;
    }
    /**
     * PROPERTY für Gehalt
     * @return
     */
    public double getGehalt() {
        return gehalt.get();
    }

    public void setGehalt(double gehalt) {
        this.gehalt.set(gehalt);
    }

    public DoubleProperty gehaltProperty() {
        return gehalt;
    }
	/**
	 *  PROPERTY für Eintrittsdatum (jeden Mitarbeiter)
	 * @return 
	 */
	public LocalDate getEintrDatum(){
		return this.eDatum.get();
	}
	public void setEintrDatum(LocalDate eintrittDatum) {
        this.eDatum.set(eintrittDatum);
    }

    public ObjectProperty<LocalDate> eintrDatumProperty() {
        return this.eDatum;
    }

	/**
	 * PROPERTIES für gebDatum
	 * @return
	 */
	public LocalDate getGebDatum() {
        return this.gDatum.get();
    }

    public void setGebDatum(LocalDate birthday) {
        this.gDatum.set(birthday);
    }

    public ObjectProperty<LocalDate> gebDatumProperty() {
        return this.gDatum;
    }

//	/**
//	 * 
//	 * @return statischer Zähler für ID - alle Mitarbeiter
//	 */
//	static int counter() {
//		return ++cnt;
//	}

	/**
	 * 
	 * @return MonatsGehalt für Mitarbeiter
	 */
	public double monGehalt() {
		return gehalt.get();
	}

	/**
	 * 
	 * @return Jahresgehalt für Mitarbeiter
	 */
	public double jahrGehalt() {
		double gehalt = this.gehalt.get() * 14;
		gehalt = Math.round(100.0 * gehalt) / 100.0;
		return gehalt;
	}

	/**
	 * 
	 * @param prozent
	 *            für Berechnung der Gehaltserhöhung Kommastellen gerundet
	 */
	public void erhoehung(double proz) {
		gehalt.set(gehalt.get() * (1 + proz / 100));
		gehalt.set(Math.round(100.0 * gehalt.get()) / 100.0);
	}

	/**
	 * 
	 * @return Anstelldauer - Periodenberechnung von .now
	 */
	public Period anstDauer() {
		LocalDate dt = LocalDate.now();
		Period dauer = Period.between(eDatum.get(), dt);
		return dauer;
	}

	/**
	 * 
	 * @return Miarbeiterblatt : toString Ausgabe
	 */
	public String mitarbBlatt() {
		return toString();

	}

	/**
	 * String toString Ausgabe für Mitarbeiter
	 */
	@Override
	public String toString() {
		DateTimeFormatter formatterOut = DateTimeFormatter.ofPattern("dd.MM.yyyy");
		String gdat = formatterOut.format(gDatum.get());
		String edat = formatterOut.format(eDatum.get());
		return String.format(
				"Mitarbeiter [id=%3s, name=%6s, gehalt=%8s, gebDatum=%12s, EintrittDatum=%12s, Jahresgehalt=%8s]", id,
				name, gehalt, gdat, edat, jahrGehalt());
	}

}
