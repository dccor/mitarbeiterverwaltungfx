package model;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;


public class MitarbeiterCellFactory {
	public static Callback<ListView<Mitarbeiter>, ListCell<Mitarbeiter>> defaultMitarbeiterDisplay(){
		
		// Den callback implementieren, den ListView beim Erzeugen neuer Zellen aufruft
		return new Callback<ListView<Mitarbeiter>, ListCell<Mitarbeiter>>() {
			
			@Override
			public ListCell<Mitarbeiter> call(ListView<Mitarbeiter> param) {
				// eigen ListCell-Klasse implementieren und zurückliefern
				return new ListCell<Mitarbeiter>(){
					@Override
					protected void updateItem(Mitarbeiter item, boolean empty) {
						// Basisklassenimplementierung aufrufen (zB: um Styles für selected  Item etc zu setzen)
						super.updateItem(item, empty);
						
						if(empty || item == null){
							// nichts anzeigen
							setText(null);
						}
						else{
							setText(item.getId() + " / " + item.getName());
						}
					}
				};
			}
		};
	}

}
