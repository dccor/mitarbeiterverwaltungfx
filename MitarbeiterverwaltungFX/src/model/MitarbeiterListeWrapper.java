package model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

public class MitarbeiterListeWrapper {
	

/**
 * Helper class to wrap a list of mitarbeiter. This is used for saving the
 * list of mitarbeiter to XML.
 * 
 * @author Dalibor Kos
 */
@XmlRootElement(name = "mitarbeiter")
public class MitarbeiterListWrapper {

    private List<Mitarbeiter> mitarbeiter;

    @XmlElement(name = "mitarbeiter")
    public List<Mitarbeiter> getMitarbeiter() {
        return mitarbeiter;
    }

    public void setMitarbeiter(List<Mitarbeiter> mitarbeiter) {
        this.mitarbeiter = mitarbeiter;
    }
}

}
