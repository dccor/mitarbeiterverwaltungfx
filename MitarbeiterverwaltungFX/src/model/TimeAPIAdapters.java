package model;


import java.time.Instant;
import java.time.LocalDate;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class TimeAPIAdapters {

    public static class LocalDateAdapter extends XmlAdapter<String, LocalDate> {

	@Override
	public String marshal(LocalDate v) throws Exception {
	    // LocalDate.toString liefert XML-konformen Datums-String
	    return v != null ? v.toString() : null;
	}

	@Override
	public LocalDate unmarshal(String v) throws Exception {
	    return v != null && !v.isEmpty() ? LocalDate.parse(v) : null;
	}

    }
    
    public static class InstantAdapter extends XmlAdapter<String, Instant>{

	@Override
	public String marshal(Instant v) throws Exception {
	    // Instant.toString liefert XML-konformen Datums-String
	    return v != null ? v.toString() : null;
	}

	@Override
	public Instant unmarshal(String v) throws Exception {
	    return v != null && !v.isEmpty() ? Instant.parse(v) : null;
	}
	
    }

}
