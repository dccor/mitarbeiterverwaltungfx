package model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Experten extends Mitarbeiter {

	/**
	 * protected String PROPERTY für Fachgebiet
	 */
	protected StringProperty fachGeb = new SimpleStringProperty("");

	/**
	 * Konstruktor für Experten
	 * 
	 * @param fachGeb
	 * @param name
	 * @param gehalt
	 * @param gebDatum
	 * @param eintrittDatum
	 */
	public Experten(int id, String fachGeb, String name, double gehalt, LocalDate gebDatum, LocalDate eintrittDatum) {
		super(id, name, gehalt, gebDatum, eintrittDatum, "Experten");
		this.fachGeb.set(fachGeb);
	}
	
	/**
	 *   Konstruktor mit mitarb vom Typ Mitarbeier
	 * @param 
	 */
	public Experten(){
		
	}

	/**
	 * 
	 * @return Mitarbeiter-Typ als int
	 */
	@Override
	public int getMitType() {
		return 2;
	}

//	/**
//	 * 
//	 * @return String Fachgebiet
//	 */
//	public String fachGebiet() {
//		return fachGeb.get();
//	}
	/**
     * PROPERTY für fachGeb
     * @return
     */
    public String getFachGeb() {
        return fachGeb.get();
    }

    public void setFachGeb(String fachGeb) {
        this.fachGeb.set(fachGeb);
    }
	public StringProperty fachGebProperty() {
        return fachGeb;
    }
	

	/**
	 * Override Methode Jahresgehalt. return Experten Jahresgehalt
	 */
	@Override
	public double jahrGehalt() {
		double jGehalt = gehalt.get() * 15;
		return jGehalt;
	}

	/**
	 * Formatierte Ausgabe Experten
	 */
	@Override
	public String toString() {
		DateTimeFormatter formatterOut = DateTimeFormatter.ofPattern("dd.MM.yyyy");
		String gdat = formatterOut.format(gDatum.get());
		String edat = formatterOut.format(eDatum.get());
		return String.format(
				"Experten    [id=%3s, name=%6s, gehalt=%8s, gebDatum=%12s, EintrittDatum=%12s, Jahresgehalt=%8s, Fachgebiet=%4s]",
				id, name, gehalt, gdat, edat, jahrGehalt(), fachGeb);
	}

}
