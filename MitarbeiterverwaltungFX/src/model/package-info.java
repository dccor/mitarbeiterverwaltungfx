@XmlAccessorType(XmlAccessType.FIELD)
@XmlSchema(namespace="mitarbeiter-namespace")
// Adapter für die Time API fürs ganze Package registrieren
@XmlJavaTypeAdapters({
    @XmlJavaTypeAdapter(TimeAPIAdapters.LocalDateAdapter.class),
    @XmlJavaTypeAdapter(TimeAPIAdapters.InstantAdapter.class)
})
package model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchema;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
