package model;

import java.time.LocalDate;
import java.util.Comparator;

public class MitarbeiterComparator implements Comparator<Mitarbeiter> {

	@Override
	public int compare(Mitarbeiter arg0, Mitarbeiter arg1) {
		int type1 = arg0.getMitType();
		int type2 = arg1.getMitType();
		if (type1 < type2) {
			return -1;
		} else if (type1 > type2) {
			return 1;
		}
		else{
			LocalDate edat1 = arg0.getEintrDatum();
			LocalDate edat2 = arg0.getEintrDatum();
			return edat1.compareTo(edat2);
		}
	}

}
