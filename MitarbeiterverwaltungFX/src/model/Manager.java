package model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class Manager extends Mitarbeiter {
	
	/**
	 *  PROPERTY für Bonus Berechnung
	 */
	protected DoubleProperty bonus = new SimpleDoubleProperty();
	
	/**
	 * Konstruktor für Manager 
	 * @param bonus
	 * @param name
	 * @param gehalt
	 * @param gebDatum
	 * @param eintrittDatum
	 */
	
	public Manager(int id,double bonus, String name, double gehalt, LocalDate gebDatum, LocalDate eintrittDatum) {
		super(id, name, gehalt, gebDatum, eintrittDatum, "Manager");
		this.bonus.set(bonus);
	}
	
	public Manager(){
		
	}
	/**
	 * 
	 * @return Mitarbeiter-Typ als int
	 */
	@Override
	public int getMitType() {
		return 1;
	}
	/**
	 *  
	 * @return  PROPERTY  Bonus Manager 
	 */
	public double getBonus(){
		return this.bonus.get();
	}
	public void setBonus(double bonus){
		this.bonus.set(bonus);
	}
	public DoubleProperty bonusProperty() {
        return bonus;
	}
	/**
	 *  Override Methoden für Manager
	 */
	@Override
	public void erhoehung(double proz) {
		super.erhoehung(proz);
		bonus.set(bonus.get() * (1 + proz / 100));
		bonus.set(Math.round(100.0 * bonus.get()) / 100.0); 
	}
	
	/**
	 * Formatierte Ausgabe Manager
	 */
	@Override
	public String toString(){
		DateTimeFormatter formatterOut = DateTimeFormatter.ofPattern("dd.MM.yyyy");
		String gdat = formatterOut.format(gDatum.get());
		String edat = formatterOut.format(eDatum.get());
		return String.format("Manager     [id=%3s, name=%6s, gehalt=%8s, gebDatum=%12s, EintrittDatum=%12s, Jahresgehalt=%8s, Bonus=%6s]"
				, id, name, gehalt, gdat, edat, jahrGehalt(), bonus);
	}
}
