package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


@Entity
@Table(name = "employees")
@NamedQuery(name = "MitarbeiterListe.findAll", query = "SELECT p FROM MitarbeiterListe p")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso(value = { Mitarbeiter.class, Manager.class, Experten.class })

public class MitarbeiterListe {

	/**
	 * observable Liste erstellen
	 */
//	@XmlElement(name = "Mitarbeiter")
//	private ObservableList<Mitarbeiter> listData = FXCollections.observableArrayList();
	@XmlElement(name = "Mitarbeiter")
	private List<Mitarbeiter> listData = new ArrayList<Mitarbeiter>();

	public MitarbeiterListe() {
		/**
		 * Attribute Datum mit formatter für Geburtsdatum und Eintrittsdatum
		 */
		DateTimeFormatter formatterOut = DateTimeFormatter.ofPattern("dd.MM.yyyy");
		LocalDate gebDatum = LocalDate.parse("05.05.1991", formatterOut);
		LocalDate eintrittDatum = LocalDate.parse("05.05.1999", formatterOut);
		/**
		 * Mitarbeiter Objekte (standardwerten)
		 */
		Mitarbeiter mitarbeiter = new Mitarbeiter(1, "Karli", 2500.55, gebDatum, eintrittDatum, "Mitarbeiter");
		Mitarbeiter manager = new Manager(2, 750, "Höller", 3900, gebDatum, eintrittDatum);
		Mitarbeiter experte = new Experten(3, "Expert", "Techni", 3200, gebDatum, eintrittDatum);
		/**
		 * Einstell- & GeburtsDatum Parameter für Manager
		 */
		manager.setGebDatum(LocalDate.parse("11.09.1988", formatterOut));
		manager.setEintrDatum(LocalDate.parse("01.01.2000", formatterOut));
		experte.setGebDatum(LocalDate.parse("30.12.1979", formatterOut));
		experte.setEintrDatum(LocalDate.parse("07.07.2010", formatterOut));
		//
		// listData.add(mitarbeiter);
		// listData.add(manager);
		// listData.add(experte);
	}

	/**
	 * getter - Returns the data as an observable list of Mitarbeiter.
	 * 
	 * @return
	 */
	public List<Mitarbeiter> getMitarbeiterData() {
		return listData;
	}

	public void setMitarbeiterData(ObservableList<Mitarbeiter> mitData) {
		this.listData = mitData;
	}

	/**
	 * boolsche Methoden zum serialisieren(read und write )
	 */
	public MitarbeiterListe readData(File fname) {

		try (InputStreamReader reader = new InputStreamReader(new FileInputStream(fname), "UTF-8")) {

			JAXBContext context = JAXBContext.newInstance(MitarbeiterListe.class);
			Unmarshaller u = context.createUnmarshaller();

			MitarbeiterListe mitarbList = (MitarbeiterListe) u.unmarshal(reader);
			listData.addAll(mitarbList.getMitarbeiterData());

			// ----------- KONTROLL --------------
			// System.out.println("mitarbList: \n" + mitarbList);
			// showAll();

			return mitarbList;

		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return null;
	}

	/**
	 * boolsche Methode bei write, marshal(this=sich selbst(@Xml Root-Element)
	 * ->ListObjekt übergeben und writen )
	 * 
	 * @param fname
	 * @return true wenn geschrieben / false wenn nicht
	 */
	public boolean writeData(String fname) {

		try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(fname), "UTF-8")) {
			JAXBContext context = JAXBContext.newInstance(MitarbeiterListe.class);
			Marshaller m = context.createMarshaller();
			m.marshal(this, writer);
			return true;
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return false;
	}

	/**
	 * get Id
	 */
	public int getHighestId() {
		int highId = 0;
		for (Mitarbeiter m : listData) {
			if (m.getId() > highId)
				highId = m.getId();
		}
		return highId;
	}

	/**
	 * sortieren mittels MitarbeiterComparator (Implementierung eines
	 * Interfaces)
	 * 
	 * @param list
	 */
	public void sortByTypEDatum() {
		MitarbeiterComparator cmp = new MitarbeiterComparator();
		listData.sort(cmp);
	}

	/**
	 * sortieren mit natürlicher Sortierreihenfolge
	 * 
	 * @param list
	 */
	public void sortList() {
		listData.sort(null);
		// comparable (compareTo) muss implementiert sein, damit ausführbar
		// dann eben mit null
	}

	/**
	 * sortieren nach ID
	 */
	public void sortById() {
		listData.sort(new Comparator<Mitarbeiter>() {
			@Override
			public int compare(Mitarbeiter m1, Mitarbeiter m2) {
				int id1 = m1.getId();
				int id2 = m2.getId();
				if (id1 == id2) {
					return 0;
				} else if (id1 < id2) {
					return -1;
				} else {
					return 1;
				}
			}
		});
	}

	/**
	 * anzeigen aller Mitarbeiter aus aktuellen(je nach Sortierung) der
	 * Mitarbeiterliste dataList
	 * 
	 * @param list
	 */
	public void showAll() {
		for (int i = 0; i < listData.size(); i++) {
			System.out.println(listData.get(i));
		}
		System.out.println("LIST AUSGABE -- Ende --");
		System.out.println();
	}

}
